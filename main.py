from pywinauto.application import Application
import time
import SimaProCOM as sp
from fileList import getInputFiles
from shutil import copy
import os

def startApp(app):
    app.About.OK.click()
    # Wait for db options to load
    time.sleep(4)
    app.Opendatabase.Open.click()

    #Wait for db to open
    time.sleep(15)
    for x in range(0,5):
        if app.Selectauser.exists():
            app.Selectauser.type_keys("{ENTER}")
            time.sleep(2)
            break
        else:
            time.sleep(2)

    # Open project
    app.top_window().Open.click()
    time.sleep(2)
    print "Project Opened"


def runCalc(app,fileName):
    #print "Running calc"

    exportCommand = ''.join(["{"+f+"}" for f in list(fileName)])
    time.sleep(180)
    for x in range(0,50):
        if app.Status.exists():
            time.sleep(5)
        else:
             # Menu File -> export
            impactWindow = app['NexusDB@10.4.45.166\Default\UARK_MAIN_ROUGE_SPv8_3; Dcap-datasmart - [Uncertainty analysis of "Raw milk, at dairy farm/US IFSM"]']['Uncertainty analysis of "Raw milk, at dairy farm/US IFSM"']
            impactWindow.ClickInput(coords=(30, 125)) #Click table output
            impactWindow.RightClickInput(coords=(40, 193)) # Open export menu in table
            impactWindow.ClickInput(coords=(85, 228)) # Click on export all
            time.sleep(2)
            app.top_window().type_keys(exportCommand) # type file name
            app.top_window().type_keys("{ENTER}") # save file export
            app.top_window().type_keys("{ENTER}") # save file export
            time.sleep(1)
            impactWindow.type_keys("{ESC}")
            time.sleep(2)
            app.Confirm.Yes.click()
            break

def main():
    # Automate GUI to enter project
    app = Application().start(r"C:\Program Files (x86)\SimaPro Multi user\simapro.exe")
    startApp(app)
    print "Reading Files"

    projectPath = "C:/Users/nstoddar/Desktop/SP-App"
    files = getInputFiles(projectPath+"/input")

    # copy file to temp, update SP inventory via COM interface, run calc via gui, export results,
    for f in files:
        # if f=="Farm-Output.csv":
        #     print f
        #     copy(os.path.join(projectPath+"/input",f),"C:/Users/nstoddar/Desktop/SP-App/temp")
        #     sp.updateInventory()
        #     runCalc(app,f.split('.')[0])
        #     print "Completed export for: " + f
        #     os.remove(os.path.join(projectPath+"/temp",f))
        copy(os.path.join(projectPath+"/input",f),"C:/Users/nstoddar/Desktop/SP-App/temp")
        sp.updateInventory()
        app['UARK_MAIN_ROUGE_SPv8_3; Dcap-datasmart'].ClickInput(coords=(400,115))
        app['UARK_MAIN_ROUGE_SPv8_3; Dcap-datasmart'].type_keys("{ENTER}")
        time.sleep(2)
        app.top_window().Calculate.click()
        runCalc(app,f.split('.')[0])
        #print "Completed export for: " + f
        os.remove(os.path.join(projectPath+"/temp",f))

main()

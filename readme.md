# Batch SimaPro

This project consists of two applications.
1. SimaPro Batch Calculation Handler (python)
2. SimaPro COM Interface Value Exchanger (c# exe)

## Getting Started
This project uses a scientific distribution of python - [Anaconda](https://anaconda.org/) - which must be installed.

### Create Dev Environment
From the project root:
- Create Environment named dev from yml
- Activate Environment
- Install requirements

```
conda env create -f environment.yml
source activate dev
pip install -r requirements.txt
```

### Running the project
```
source activate dev
python main.pys
```

### Inputs/Output
The COM executable - BatchSP - looks for a child folder named 'temp-input'. Please note, files in 'temp-input' will be deleted after calculation.

Once the calculation handler completed sensitivity analysis for a given solution the calculation ouput is saved to the Default SimaPro location C:/Users/nstoddar/Documents

from os import listdir
from os.path import isfile, join

def getInputFiles(inputPath):
    return [f for f in listdir(inputPath) if isfile(join(inputPath, f))]
